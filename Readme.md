# Einleitung

dfgsfgdgsd ,,das sind Anführungszeichen'' und das ist **fetter Text** und das ist *kursiver Text*.

- Auflistung
- Auflistung
- Auflistung
- Auflistung

Mit Nummern:

1.  hallo 1
2.  hallo 2
7999.  hallo 3
9.  hallo 4

und das hier:

> Zitaslöas dsakflkdn d fkvl dlkfsv kdlf vlkd
> Zitaslöas dsakflkdn d fkvl dlkfsv kdlf vlkd

code wäre so

\begin{cppcode}
if (;;) {
	x[q] = y+1;
}
\end{cppcode}

Man kann auch diese `diktengleiche` Schrift nehmen.

# Design Phil von DARPA

![Ein Bild hurra](images/logo.png)

blah blah

# Verstopfungsvermeidung und Kontrolle

[Link zum Paper](https://people.eecs.berkeley.edu/~sylvia/papers/congavoid.pdf)

## Ein öäü test Tabelle. Der Doppelpunkt bestimmt Ausrichtung!

| Instanz          | Zeiträume | Projekte |    Laufzeit |
|:-----------------|----------:|---------:|------------:|
| OR5x500-0.50_1   |  5        | 500      |       4.399 |
| OR10x500-0.75_1  | 10        | 500      |      97.199 |
| OR10x500-0.75_4  | 10        | 500      |     353.366 |

Gleichzeitig
:   a und b ist gleichzeitig, wenn kein Nachrichtenaustausch zwischen a und b 
passiert und daher keine Aussage getroffen werden kann, ob a vor b, 
oder b vor a passiert. Bei Vektoruhren liefert before(a,b) und 
before(b,a) jeweils NICHT.

schwaches Konsistenzkriterium (oder nur "Clock Condition")
:    gilt $a \rightarrow b$ dann folgt daraus, dass die Uhr von a einen kleineren Wert hatte, als die von b. VORSICHT ! Das bedeutet nicht, dass a "Happend Before" b gilt!

Eine Notiz

>   Provided they touch only memory owned by a single
>   core, we can use them to perform simple *read/write*
>   operations to global memory, as well as more
>   complex *read-modify-write* operations (e.g., *fetch-and-add*).

# Aufgabe 1

VPN war was knifflig, tat aber dann

# Aufgabe 2a

Mit `Semi` lassen sich die Variablen auf kontinuierlich setzen.

    Maximize
      2 x1 + 4 x2 - 5 x3 - x4 + x5
    s.t.
    c0:  2 x1 + 2 x2 +   x3               <= 5
    c1:         3 x2        - 2 x4 + 3 x5 <= 2
    c2:    x1        + 4 x3 - 3 x4 + 2 x5 <= 13
    c3:    x1   + x2 +   x3   + x4        <= 8
    c4:  2 x1        - 2 x3               <= 4
    c5: -1 x1 + 6 x2               + 2 x5 <= 2
    Bounds
      x1 >= 0
      x2 >= 0
      x3 >= 0
      x4 >= 0
      x5 >= 0
    Semi
      x1 x2 x3 x4 x5
    End

Aufruf der Datei und schreiben der Lösung mit `gurobi_cl ResultFile=ueb2_au2_a.sol ueb2_au2_a.lp`

    Academic license - for non-commercial use only
    
    Gurobi Optimizer version 7.5.1 build v7.5.1rc0 (linux64)
    Copyright (c) 2017, Gurobi Optimization, Inc.
    
    Read LP format model from file ueb2_au2_a.lp
    Reading time = 0.01 seconds
    : 6 rows, 5 columns, 19 nonzeros
    Optimize a model with 6 rows, 5 columns and 19 nonzeros
    Variable types: 0 continuous, 0 integer (0 binary)
    Semi-Variable types: 5 continuous, 0 integer
    Coefficient statistics:
      Matrix range     [1e+00, 6e+00]
      Objective range  [1e+00, 5e+00]
      Bounds range     [0e+00, 0e+00]
      RHS range        [2e+00, 1e+01]
    Found heuristic solution: objective -0
    Presolve removed 2 rows and 0 columns
    Presolve time: 0.01s
    Presolved: 4 rows, 5 columns, 11 nonzeros
    Found heuristic solution: objective 3.0000000
    Variable types: 5 continuous, 0 integer (0 binary)
    
    Root relaxation: objective 6.300000e+00, 4 iterations, 0.00 seconds
    
        Nodes    |    Current Node    |     Objective Bounds      |     Work
     Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time
    
    *    0     0               0       6.3000000    6.30000  0.00%     -    0s
    
    Explored 0 nodes (4 simplex iterations) in 0.02 seconds
    Thread count was 2 (of 2 available processors)
    
    Solution count 2: 6.3 3 
    
    Optimal solution found (tolerance 1.00e-04)
    Best objective 6.300000000000e+00, best bound 6.300000000000e+00, gap 0.0000%
    
    Wrote result file 'ueb2_au2_a.sol'

Die Lösung steht in `ueb2_au2_a.sol`. Das Maximum ist bei ca 6,3 und
wird mit ca. $x_{1} = 1,866$, $x_{2} = 0,633$, $x_{3} = x_{4} = 0$
und $x_{5} = 0,0333$ erreicht

    # Objective value = 6.3000000000000007e+00
    x1 1.8666666666666667e+00
    x2 6.3333333333333341e-01
    x3 0
    x4 0
    x5 3.3333333333333423e-02


1.  Autoren beobachteten, dass 10% der Pakets am Gateway wegen 
    überfüllter Buffer gedroppt wird
2.  es gab 1986 einen Crash: 32000bps auf 40bps
3.  Problem liegt in TCP Protokollimplementierung
4.  Verbesserung 4BSD TCP in diesem Paper beschrieben:
    i.  diese Auflistung geht in html bzw gitlab nicht.
        diese Auflistung geht in html bzw gitlab nicht.
    ii. slow-start
    iii. dynamisch Fensteranpassung bei Verstopfung
5.  Gründe dafür, dass die Sender-Empfänger-Sender Schleife und 
    das ,,Konservieren der Pakete in der Leitung'' gestört ist:
    23. Das wird in gitlab nicht autom. durchnummeriert! aber im pdf!
    #.  der Sender fügt bereits Pakete hinzu, obwohl alte Pakete noch 
        nicht angekommen sind
    #.  durch Resourcenlimit entlang des Pfads kann nie eine 
        gleichmäßige Verbindung erreicht werden

$$ \frac{\epsilon}{1- \kappa} \leq \mu $$

hallo welt

# Code 1

Eine Möglichkeit, aber fürs Web doof (Einrückung mit 4 Leerzeichen eigentlich best Practice):

~~~cpp
// tutorial/hello_world_2.cpp
#include <Grappa.hpp>
#include <Collective.hpp>
#include <iostream>
int main(int argc, char *argv[]) {
  Grappa::init(&argc, &argv);
  Grappa::run([]{
    std::cout << "Hello world from the root task!\n";
    Grappa::on_all_cores([]{
      std::cout << "Hello world from Core "
                << Grappa::mycore() << " of " << Grappa::cores()
                << " (locale " << Grappa::mylocale() << ")"<< "\n";
    });
    std::cout << "Exiting root task.\n";
  });
  Grappa::finalize();
}
~~~

Und java tut auch

~~~java
import gurobi.*;
 
public class Ueb2Au3a {
  private static double _solution;
  
  public static void main(String[] args) {
    if (args.length != 1) {
        System.out.println("datei vergessen");
        return;
    }
    
    try {
        GRBEnv env = new GRBEnv();
        env.set(GRB.IntParam.OutputFlag, 0);
        GRBModel model = new GRBModel(env, args[0]);
        
        // nein. ein neues Model als Datei einlesen geht nur mit Constructor
        //model.read(args[0]);
        
        model.optimize();
        
        _solution = model.get(GRB.DoubleAttr.ObjVal);
        System.out.print("Lösung: ");
        System.out.println(_solution);
        
        for (GRBVar v : model.getVars()) {
            double xsol = v.get(GRB.DoubleAttr.X);
            System.out.print(v.get(GRB.StringAttr.VarName));
            System.out.print(" = ");
            System.out.println(xsol);
        }
      
        model.dispose();
        env.dispose();
    
    } catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
          e.getMessage());
    }
  }
}
~~~


# Code 2

Mit `sed` ist auch ein replace von 3 Tilde + cpp und 3 Tilde ohne cpp
möglich.

~~~cpp
class Spinlock {
    
private:
    unsigned int lock_var;
    unsigned int *ptr;
    
    Spinlock(const Spinlock &copy);   // Verhindere Kopieren
    
public:
    Spinlock() {
        lock_var = 0;
        ptr  = &lock_var;
    }
    
    // Spinlock anfordern
    void lock();
    
    // Spinlock freigeben
    void unlock();
    
};
~~~

Sowas mit sed kann natürlich auch schief laufen.
